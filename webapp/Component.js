sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"test/dore/charts/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("test.dore.charts.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
			this.getRouter().initialize();
		},
		
		getContentDensityClass: function() {
			if (this._sContentDensityClass === "undefined") {
				if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
					this._sContentDensityClass = "";
				} else if (!Device.support.touch) {
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this.__sContentDensityClass;
		}
	});
});