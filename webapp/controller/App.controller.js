sap.ui.define([
	"test/dore/charts/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function(BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("test.dore.charts.controller.App", {

		onInit: function() {
			var oViewModel,
				fnSetAppNotBusy,
				iOriginalDelay = this.getView().getBusyIndicatorDelay();
				
			oViewModel = new JSONModel({
				busy: true,
				delay: 0
			});
			this.setModel(oViewModel, "appView");
			
			fnSetAppNotBusy = function() {
				oViewModel.setProperty("/busy", false);
				oViewModel.setProperty("/delay", iOriginalDelay);
			};
			this.getOwnerComponent().getModel().metadataLoaded().then(fnSetAppNotBusy);
			
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
		}

	});
});