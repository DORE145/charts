sap.ui.define([
	"sap/viz/ui5/data/FlattenedDataset",
	"sap/viz/ui5/data/DimensionDefinition",
	"sap/viz/ui5/data/MeasureDefinition",
	"sap/viz/ui5/controls/common/feeds/FeedItem",
	"sap/ui/model/Filter",
	"test/dore/charts/model/formatter",
	"test/dore/charts/controller/BaseController"
	], function(FlattenedDataset, DimensionDefinition, MeasureDefinition, FeedItem, Filter, formatter, BaseController) {
		"use strict";
		
		return {
			
			formatter: formatter,
			BaseController: BaseController,
			
			_createInstanceDimensionDefinition: function() {
				return new DimensionDefinition({
						name: "Instance",
						value: "{AS_Instance}"
					});
			},
			
			_createTimeDimensionDefinition: function() {
				return new DimensionDefinition({
						name: "Time",
						dataType: "date",
						value: {
							path: "Time",
							formatter: formatter.timeFormat
						}
					});
			},
			
			createColorFeedItem: function() {
				return new FeedItem({
						uid: "color",
						type: "Dimension",
						values: ["Instance"]
					});
			},
			
			createTimeFeedItem: function() {
				return new FeedItem({
						uid: "timeAxis",
						type: "Dimension",
						values: ["Time"]
					});
			},
			
			createMeasureDefenition: function(sName, sValue) {
				return new MeasureDefinition({
					name: sName,
					value: sValue
				});
			},
			
			createFlattenedDataset: function(aMeasureDefenitions, iId, oSettingsModel) {
				var sPath,
					aInstances = oSettingsModel.getProperty("/aInstances"),
					sAggregation = oSettingsModel.getProperty("/sAggregation"),
					aInstancesFilters = [];
				if (sAggregation === "0") {
					sPath = "/SelectionDataParams(Id=" + iId + ")/Results";
				} else {
					sPath = "/AggregatedDataParams(Id=" + iId + ",agregation='INTERVAL%20" + sAggregation +
								"%20MINUTE')/Results";
				}
				aInstances.forEach(function (item) {
					var oFilter = new Filter("AS_Instance", "EQ", item);
					aInstancesFilters.push(oFilter);
				});
				var oInstanceFilter = new Filter({
					filters: aInstancesFilters,
					and: false
				});
				return new FlattenedDataset({
					dimensions: [
						this._createInstanceDimensionDefinition(),
						this._createTimeDimensionDefinition()
					],
					measures: aMeasureDefenitions,
					data: {
						path: sPath,
						filters: oInstanceFilter
					}
				});
			},
			
			updateFlattenedDataset: function(oChart, iId, oSettingsModel) {
				var oOldDataset = oChart.getDataset(),
					oDimensions = oOldDataset.getDimensions(),
					oMeasures = oOldDataset.getMeasures(),
					aInstances = oSettingsModel.getProperty("/aInstances"),
					sAggregation = oSettingsModel.getProperty("/sAggregation"),
					iTimeFrom = oSettingsModel.getProperty("/iTimeFrom"),
					iTimeTo = oSettingsModel.getProperty("/iTimeTo"),
					aInstancesFilters = [],
					vizProperties = {};
					
				aInstances.forEach(function (item) {
					var oFilter = new Filter("AS_Instance", "EQ", item);
					aInstancesFilters.push(oFilter);
				});
				var oInstanceFilter = new Filter({
					filters: aInstancesFilters,
					and: false
				}),
					oTimeFilter = new Filter("Time", function (oValue) {
							if (oValue.ms >= iTimeFrom && oValue.ms <= iTimeTo) {
								return true;
							}
							return false;
						}
					),
					oFilters = new Filter({
						filters: [oInstanceFilter, oTimeFilter],
						and: true
					}),
					sPath;
				if (sAggregation === "0") {
					sPath = "/SelectionDataParams(Id=" + iId + ")/Results";
					vizProperties = {
						title: {
							visible: false,
							text: ""
						}
					};
				} else {
					sPath = "/AggregatedDataParams(Id=" + iId + ",agregation='INTERVAL%20" + sAggregation +
						"%20MINUTE')/Results";
					vizProperties = {
						title: {
							visible: true,
							text: "Aggreated by " + sAggregation + " minute(s)"
						}
					};
				}
				oChart.setVizProperties(vizProperties);
				var newDataset = new FlattenedDataset({
						dimensions: oDimensions,
						measures: oMeasures,
						data: {
							path: sPath,
							filters: oFilters,
							parameters: {operationMode: "Client"}
						}
					});
				oChart.setDataset(newDataset);
			},
			
			createFeedItem: function(oMeasureDefenition) {
				return new FeedItem({
					uid: "valueAxis",
					type: "Measure",
					values: [oMeasureDefenition.getName()]
				});
			}
			
		};
	}
);