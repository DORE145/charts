sap.ui.define([
	"test/dore/charts/controller/BaseController",
	"test/dore/charts/controller/ChartsHelper",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/Filter",
	"sap/ui/core/routing/History",
	"test/dore/charts/model/formatter"
	], function(BaseController, ChartsHelper, JSONModel, ODataModel, Filter, History, formatter) {
		"use strict";
		
		return BaseController.extend("test.dore.charts.controller.ObjectCharts", {
			
			formatter: formatter,
			ChartsHelper: ChartsHelper,
			
			onInit: function() {
				var oViewModel = new JSONModel({
						busy: true,
						delay: 0
					}),
					recordInfo = new JSONModel({
						Id: "",
						CustomerName: "",
						date: "",
						source: "",
						serviceType: "",
						SID: "",
						Instances: []
					}),
					defaultChartSettings = new JSONModel({
						aInstances: [],
						sAggregation: "",
						iTimeFrom: "",
						iTimeTo: ""
					}),
					chartsSettings = new JSONModel();
				this.setModel(oViewModel, "chartsView");
				this.setModel(recordInfo, "recordInfo");
				this.setModel(defaultChartSettings, "defaultChartSettings");
				this.setModel(chartsSettings, "chartsSettings");
				
				this._bRecordsLoaded = false;
				this._bInstancesLoaded = false;
				
				this.getRouter().getRoute("objectCharts").attachPatternMatched(this._onObjectMatched, this);
				this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
				this._populateCustomChartSelect();
			},
			
			_onObjectMatched: function(oEvent) {
				var sObjectId = oEvent.getParameter("arguments").objectId,
					oModel = new ODataModel("/destinations/hanaStats/", {
						disableHeadRequestForToken: true	
					}),
					oFilter = new Filter("Id", "EQ", sObjectId);
					this.getView().byId("objectHeader").setBusy(true);
				this._oModel = oModel;
					
					oModel.read("/records", {
						filters: [oFilter],
						success: function(responce) {
							var result = responce.results[0],
								recordInfo = this.getModel("recordInfo"),
								defaultSettings = this.getModel("defaultChartSettings");
							this._Id = result.Id;
							recordInfo.setProperty("/Id", result.Id);
							recordInfo.setProperty("/CustomerName", result.CustomerName);
							recordInfo.setProperty("/date", this.formatter.mediumDate(result.Date));
							recordInfo.setProperty("/source", result.Source);
							recordInfo.setProperty("/serviceType", result.ServiceType);
							recordInfo.setProperty("/SID", result.SID);
							defaultSettings.setProperty("/sAggregation", result.Aggregation);
							defaultSettings.setProperty("/iTimeFrom", result.TimeFrom.ms);
							defaultSettings.setProperty("/iTimeTo", result.TimeTo.ms);
							this.getView().byId("objectHeader").setBusy(false);
							this._bRecordsLoaded = true;
							this._loadChartsData();
						}.bind(this)
					});
					oModel.read("/instances", {
						filters: [oFilter],
						urlParameters: {"$select": "AS_Instance"},
						success: function (responce) {
							var result = responce.results,
								instances = [];
							result.forEach(function(item) {
								instances.push(item.AS_Instance);
							}, this);
						this.getModel("recordInfo").setProperty("/Instances", instances);
						this.getModel("defaultChartSettings").setProperty("/aInstances", instances);
						this._bInstancesLoaded = true;
						this._loadChartsData();
						}.bind(this)
					});
			},
			
			_onMetadataLoaded: function() {
				var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
				oViewModel = this.getModel("chartsView");
				oViewModel.setProperty("/delay", 0);
				oViewModel.setProperty("/busy", false);
				oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
			},
			
			_loadChartsData: function() {
				if (this._bRecordsLoaded && this._bInstancesLoaded) {
					this._vizProperties = {
					legend: {
						isScrollable: true
					},
					plotArea: {
						isSmoothed: true,
						gap: {
							visible: false
						},
						window: {
							start: "firstDataPoint",
							end: "lastDataPoint"
						}
					},
					timeAxis: {
						title: {visible: "true", text: "Time"},
						levels: ["second", "minute", "hour"]
					},
					title: {
						visible: true,
						text: "",
						alignment: "left",
						layout: {
							height: 20
						},
						style: {
							color: "#948A81",
							fontSize: "12px",
							fontWeight: "normal"
						}
					}
				};
					//Cpu charts
					this._bindData("cpuActive", "CPU_ACT", 0, 100);
					this._bindData("cpuIdle", "CPU_IDLE", 0, 100);
					
					//Wps charts
					this._bindData("avalRfc", "RFC_WPS");
					this._bindData("actWps", "ACT_WPS");
					this._bindData("diaWps", "DIA_WPS");
					
					//Memory charts
					this._bindData("FreeMem", "FREE_MEM");
					this._bindData("heap", "HEAP");
					this._bindData("pagingIn", "PAGING_IN");
					this._bindData("pagingOut", "PAGING_OUT");
					
					//Users charts
					this._bindData("sessions", "SESSIONS");
					this._bindData("logins", "LOGINS");
					
					//Custom chart
					this._bindData("custom", "ACT_WPS");
				}
			},
			
			_bindData: function(sContainerId, sValueLocation, iMinValue, iMaxValue) {
				this._initializeChart(sContainerId + "Chart", sValueLocation, iMinValue, iMaxValue);
				this._initializeTable(sContainerId + "Table", sValueLocation);
			},
			
			_initializeChart: function(sChartId, sValueLocation, iMinValue, iMaxValue) {
				var oChart = this.getView().byId(sChartId),
					oDefaultSettingsModel = this.getModel("defaultChartSettings"),
					oDefaultSettingsJSON = oDefaultSettingsModel.getJSON(),
					oCurrentModel = new JSONModel();
				oCurrentModel.setJSON(oDefaultSettingsJSON);
				var	aCurrentIntsances = oCurrentModel.getProperty("/aInstances"),
					sAggregation = oCurrentModel.getProperty("/sAggregation"),
					aNewInstances = [],
					sValueName = this.getResourceBundle().getText(sValueLocation + "AxisTitle"),
					bGlobalOnly = false;
				if (sValueLocation === "ENQ_ENTRIES" || sValueLocation === "IN_QUEUE" || sValueLocation === "OUT_QUEUE") {
					bGlobalOnly = true;
				}
				aCurrentIntsances.forEach(function(item) {
					if (item.toLowerCase().includes("global") === bGlobalOnly) {
						aNewInstances.push(item);
					}
				});
				oCurrentModel.setProperty("/aInstances", aNewInstances);
				this._setSettings(oCurrentModel.getJSON(), sChartId);
				var oMeasureDefenition = ChartsHelper.createMeasureDefenition(sValueName, "{" + sValueLocation + "}"),
					oFlattenedDataset = ChartsHelper.createFlattenedDataset([oMeasureDefenition], this._Id, oCurrentModel),
					oFeedItem = ChartsHelper.createFeedItem(oMeasureDefenition);
				oChart.removeAllFeeds();
				oChart.setDataset(oFlattenedDataset);
				oChart.setModel(this._oModel);
				oChart.setVizProperties(this._vizProperties);
				oChart.addFeed(ChartsHelper.createColorFeedItem());
				oChart.addFeed(ChartsHelper.createTimeFeedItem());
				oChart.addFeed(oFeedItem);
				oChart.setVizProperties({
						title: {
							visible: true,
							text: this.getResourceBundle().getText("chartsAggregation", sAggregation)
						}
					});
				if (typeof iMinValue !== "undefined") {
					oChart.setVizProperties({
						plotArea: {
							primaryScale: {
								fixedRange: true,
								minValue: iMinValue
							}
						}
					});
				}
				if (typeof iMaxValue !== "undefined") {
					oChart.setVizProperties({
						plotArea: {
							primaryScale: {
								fixedRange: true,
								maxValue: iMaxValue
							}
						}
					});
				}
			},
			
			_initializeTable: function(sTableId, sValueLocation) {
				var oTable = this.getView().byId(sTableId),
					iId = this.getModel("recordInfo").getProperty("/Id"),
					oFilter = new Filter("Id", "EQ", iId),
					sTimeLable = this.getResourceBundle().getText("timeColumnLabel"),
					sInstanceLable = this.getResourceBundle().getText("instanceColumnLabel"),
					sValueName = this.getResourceBundle().getText(sValueLocation + "AxisTitle");
				oTable.removeAllColumns();
				oTable.addColumn(new sap.ui.table.Column({
						label: sTimeLable,
						template: new sap.m.Text({text:  {
							path: "Time",
							formatter: formatter.timeToString
							}
						})
					})
				);
				oTable.addColumn(new sap.ui.table.Column({
					label: sInstanceLable,
					template: new sap.m.Text({text: "{AS_Instance}"})
				}));
				oTable.addColumn(new sap.ui.table.Column({
					label: sValueName,
					template: new sap.m.Text({text: "{" + sValueLocation + "}"})
				}));
				
				
				oTable.setModel(this._oModel);
				oTable.bindRows({
					path: "/data",
					filters: oFilter,
					sorter: new sap.ui.model.Sorter("Time")
				});
			},
			
			_setSettings: function(oChartSettings, sChartId) {
				var oChartSettingsModel = this.getModel("chartsSettings");
				oChartSettingsModel.setProperty("/" + sChartId, oChartSettings);
			},
			
			_setSettingsForAll: function(oChartSettings) {
				this._setSettings(oChartSettings, "cpuActiveChart");
				this._setSettings(oChartSettings, "cpuIdleChart");
				this._setSettings(oChartSettings, "avalRfcChart");
				this._setSettings(oChartSettings, "actWpsChart");
				this._setSettings(oChartSettings, "diaWpsChart");
				this._setSettings(oChartSettings, "FreeMemChart");
				this._setSettings(oChartSettings, "heapChart");
				this._setSettings(oChartSettings, "pagingInChart");
				this._setSettings(oChartSettings, "pagingOutChart");
				this._setSettings(oChartSettings, "sessionsChart");
				this._setSettings(oChartSettings, "loginsChart");
				this._setSettings(oChartSettings, "customChart");                                  
			},
			
			onNavBack: function() {
				var sPreviousHash = History.getInstance().getPreviousHash();
				if (this.oPersonalizateDialog) {
					this.getView().removeDependent(this.oPersonalizateDialog);
					this.oPersonalizateDialog.destroy();
					this.oPersonalizateDialog = null;
				}
				if (sPreviousHash !== undefined) {
					history.go(-1);
				} else {
					this.getRouter().navTo("recordsList", {}, true);
				}
				
			},
			
			onPersonalizationPress: function(oControlEvent) {
				if (!this._bRecordsLoaded || !this._bInstancesLoaded) return;
				var sChartsId = oControlEvent.getSource().getContent()[0].getContent().getId().split("--")[2],
				oChartSettingsModel = new JSONModel();
				oChartSettingsModel.setJSON(this.getModel("chartsSettings").getProperty("/" + sChartsId));
				this._currentChartId = sChartsId;
				this._currentChartSettings = oChartSettingsModel;
				var oView = this.getView();
				if (!this.oPersonalizateDialog) {
					this.oPersonalizateDialog = sap.ui.xmlfragment(oView.getId(), "test.dore.charts.view.PersonalizationDialog", this );
					oView.addDependent(this.oPersonalizateDialog);
					var oInstanceCheckboxContainer = oView.byId("PersonalizationInstancesForm"),
						aInstances = this.getModel("recordInfo").getProperty("/Instances");
					aInstances.forEach(function (element) {
						var oChekhbox = new sap.m.CheckBox({
							text: element,
							name: element,
							selected: true
						});
					oInstanceCheckboxContainer.addContent(oChekhbox);
					});
				}
				this._setSettingInDialog(this._currentChartSettings);
				
				this.oPersonalizateDialog.open();
			},
			
			_setSettingInDialog: function(oSettingsModel) {
				var oView = this.getView(),
					oCheckboxContainer = oView.byId("PersonalizationInstancesForm"),
					aCheckBoxes = oCheckboxContainer.getContent(),
					oAggregationInput = oView.byId("PersonalizationDialogAggregationInput"),
					oTimePickerFrom = oView.byId("PersonalizationTimePickerFrom"),
					oTimePickerTo = oView.byId("PersonalizationTimePickerTo"),
					aInstances = oSettingsModel.getProperty("/aInstances"),
					sAggregation = oSettingsModel.getProperty("/sAggregation"),
					iTimeFrom = oSettingsModel.getProperty("/iTimeFrom"),
					iTimeTo = oSettingsModel.getProperty("/iTimeTo");
					
					aCheckBoxes.forEach(function (item) {
						var state = aInstances.includes(item.getText());
						item.setSelected(state);
					});
					oAggregationInput.setValue(sAggregation);
					var iTimeZoneOffset = new Date().getTimezoneOffset() * 60 * 1000;
					oTimePickerFrom.setDateValue(new Date(iTimeFrom + iTimeZoneOffset));
					oTimePickerTo.setDateValue(new Date(iTimeTo + iTimeZoneOffset));
			},
			
			onApplySettings: function(sChartId) {
				var oView = this.getView(),
					oCheckboxContainer = oView.byId("PersonalizationInstancesForm");
				if (oCheckboxContainer === undefined) return;
				var	aCheckBoxes = oCheckboxContainer.getContent(),
					oAggregationInput = oView.byId("PersonalizationDialogAggregationInput"),
					oTimePickerFrom = oView.byId("PersonalizationTimePickerFrom"),
					oTimePickerTo = oView.byId("PersonalizationTimePickerTo"),
					oNewSettingsModel = new JSONModel(),
					aInstances = [],
					iTimeOffset = new Date().getTimezoneOffset() * 60 * 1000,
					iTimeFrom = oTimePickerFrom.getDateValue().getTime() - iTimeOffset,
					iTimeTo = oTimePickerTo.getDateValue().getTime() - iTimeOffset;
				
				aCheckBoxes.forEach(function(item) {
					if (item.getSelected()) {
						aInstances.push(item.getName());
					}	
				});
				oNewSettingsModel.setProperty("/aInstances", aInstances);
				oNewSettingsModel.setProperty("/sAggregation", oAggregationInput.getValue());
				oNewSettingsModel.setProperty("/iTimeFrom", iTimeFrom);
				oNewSettingsModel.setProperty("/iTimeTo", iTimeTo);
				
				this.getModel("chartsSettings").setProperty("/" + sChartId, oNewSettingsModel.getJSON());
				
				var oChart = oView.byId(sChartId);
				ChartsHelper.updateFlattenedDataset(oChart, this._Id, oNewSettingsModel);
			},
			
			onApplySettingsForAll: function() {
				this.onApplySettings("cpuActiveChart");
				this.onApplySettings("cpuIdleChart");
				this.onApplySettings("avalRfcChart");
				this.onApplySettings("actWpsChart");
				this.onApplySettings("diaWpsChart");
				this.onApplySettings("FreeMemChart");
				this.onApplySettings("heapChart");
				this.onApplySettings("pagingInChart");
				this.onApplySettings("pagingOutChart");
				this.onApplySettings("sessionsChart");
				this.onApplySettings("loginsChart");
				this.oPersonalizateDialog.close();
			},
			
			onApplySettingsForThis: function() {
				this.onApplySettings(this._currentChartId);
				this.oPersonalizateDialog.close();
			},
			
			onPersonalizationReset: function() {
				this._setSettingInDialog(this.getModel("defaultChartSettings"));
			},
		
			onPersonlizationCancel: function() {
				this.oPersonalizateDialog.close();
			},
			
			_populateCustomChartSelect: function() {
				var oSelect = this.getView().byId("customChartSelector");
				oSelect.removeAllItems();
				oSelect.addItem(new sap.ui.core.Item({text: "Active WPs", key: "ACT_WPS"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Active DIA WPs", key: "DIA_WPS"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Avaliable RFC WPs", key: "RFC_WPS"}));
				oSelect.addItem(new sap.ui.core.Item({text: "CPU Active", key: "CPU_ACT"}));
				oSelect.addItem(new sap.ui.core.Item({text: "CPU User", key: "CPU_USR"}));
				oSelect.addItem(new sap.ui.core.Item({text: "CPU System", key: "CPU_SYS"}));
				oSelect.addItem(new sap.ui.core.Item({text: "CPU Idle", key: "CPU_IDLE"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Avaliable CPU", key: "AVA_CPU"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Paging In", key: "PAGING_IN"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Paging Out", key: "PAGING_OUT"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Free Memory", key: "FREE_MEM"}));
				oSelect.addItem(new sap.ui.core.Item({text: "EM Allocated", key: "EM_ALLOC"}));
				oSelect.addItem(new sap.ui.core.Item({text: "EM Attached", key: "EM_ATTACH"}));
				oSelect.addItem(new sap.ui.core.Item({text: "EM Global", key: "EM_GLOBAL"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Heap", key: "HEAP"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Priv", key: "PRIV"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Page SHM", key: "PAGE_SHM"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Roll SHM", key: "ROLL_SHM"}));
				oSelect.addItem(new sap.ui.core.Item({text: "DIA Queue", key: "DIA_QUEUE"}));
				oSelect.addItem(new sap.ui.core.Item({text: "UPD Queue", key: "UPD_QUEUE"}));
				oSelect.addItem(new sap.ui.core.Item({text: "ENQ Queue", key: "ENQ_QUEUE"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Sessions", key: "SESSIONS"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Logins", key: "LOGINS"}));
				oSelect.addItem(new sap.ui.core.Item({text: "ENQ Entries", key: "ENQ_ENTRIES"}));
				oSelect.addItem(new sap.ui.core.Item({text: "In Queue", key: "IN_QUEUE"}));
				oSelect.addItem(new sap.ui.core.Item({text: "Out Queue", key: "OUT_QUEUE"}));
			},
			
			onDimensionChange: function(oEvent) {
				var sKey = oEvent.getParameters().selectedItem.getKey();
				this._initializeChart("customChart", sKey);
				this.onApplySettings("customChart");
			},
			
			onContentChange: function(oEvent) {
				var sContentId = oEvent.getParameters().selectedItemId,
					bTable = sContentId.includes("Table"),
					oChartContainer = this.getView().byId(oEvent.getParameters().id);
				oChartContainer.setShowPersonalization(!bTable);
			},
			
			onDowloadChart: function(oEvent) {
				var iWidth = 1920,
					iHeigth = 1080,
					oVizFrame = oEvent.getSource().getParent().getContent()[0].getContent(),
					sSvgString = oVizFrame.exportToSVGString({
						width: iWidth,
						height: iHeigth
					}),	
					sSvgSrc = "data:image/svg+xml;base64," + btoa(sSvgString),
					sName = oVizFrame.getId().split("-").pop(),
					oImg = new Image(),
					oCanvas = document.createElement("canvas"),
    				oContext = oCanvas.getContext("2d");
    				oCanvas.width = iWidth;
    				oCanvas.height = iHeigth;
				oImg.onload = function() {
					oContext.fillStyle = "white";
					oContext.fillRect(0, 0, oCanvas.width, oCanvas.height);
					oContext.drawImage(oImg, 0, 0);
				    var oImgURI = oCanvas
				        .toDataURL("image/png")
				        .replace("image/png", "image/octet-stream");
				    
					var oMouseEvent = new MouseEvent("click", {
					    view: window,
					    bubbles: false,
					    cancelable: true
					  });
					
					  var a = document.createElement('a');
					  a.setAttribute("download", sName + ".png");
					  a.setAttribute("href", oImgURI);
					  a.setAttribute("target", "_blank");
					
					  a.dispatchEvent(oMouseEvent);
				};
				oImg.src = sSvgSrc;
				
			},
			
			onPersonalizationSelectAll: function() {
				var oCheckboxContainer = this.getView().byId("PersonalizationInstancesForm");
				if (oCheckboxContainer === undefined) return;
				var	aCheckBoxes = oCheckboxContainer.getContent();
				aCheckBoxes.forEach(function(item) {
					item.setSelected(true);	
				});
			}, 
			
			onPersonalizationHideAll: function() {
				var oCheckboxContainer = this.getView().byId("PersonalizationInstancesForm");
				if (oCheckboxContainer === undefined) return;
				var	aCheckBoxes = oCheckboxContainer.getContent();
				aCheckBoxes.forEach(function(item) {
					item.setSelected(false);	
				});
			}
		});
	}
);