sap.ui.define([
	"test/dore/charts/controller/BaseController",
	"sap/ui/model/Filter",
	"test/dore/charts/model/formatter"
	], function(BaseController, Filter, formatter) {
		"use strict";
		
		return BaseController.extend("test.dore.charts.controller.RecordsList", {
			
			formatter: formatter,
			
			SERVER_ADDRESS: "https://chartsi335911trial.hanatrial.ondemand.com/chartsServ/",
			
			handleItemSelect: function(oEvent) {
				var objectId = oEvent.getParameter("listItem").getCells()[0].getText();
				this.getRouter().navTo("objectCharts", {
					objectId: objectId
				});
			},
			
			showUploadDialog: function() {
				var oView = this.getView(),
					oDialog = oView.byId("uploadDialog");
				if (!oDialog) {
					oDialog = sap.ui.xmlfragment(oView.getId(), "test.dore.charts.view.UploadDialog", this );
					oView.addDependent(oDialog);
				}
				
				oDialog.open();
			},
			
			onDeleteButtonPressed: function(oEvent) {
				var oTable = this.getView().byId("recordsTable");
				if (oTable.getSelectedItems().length === 0) return;
				jQuery.sap.require("sap.m.MessageBox");
				sap.m.MessageBox.show(
	    			this.getResourceBundle().getText("deleteConformationMessage"), {
		        	icon: sap.m.MessageBox.Icon.CONFIRM,
		        	title: this.getResourceBundle().getText("deleteConformationTitle"),
		        	actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
		        	onClose: function(oAction) { 
		        		if(oAction === sap.m.MessageBox.Action.YES) {
		        			oTable.getSelectedItems().forEach(function(item) {
								this._deleteData(item.getCells()[0].getText());
							}.bind(this));		
		        		}  
		        	}.bind(this)
	    		});
			},
			
			showCopyFromCplipboardDialog: function() {
				var oView = this.getView(),
					oDialog = oView.byId("importFromClipboardDialog");
				if (!oDialog) {
					oDialog = sap.ui.xmlfragment(oView.getId(), "test.dore.charts.view.ImportFromClipboard", this);
					oView.addDependent(oDialog);
				}
				oDialog.open();
			},
			
			onSearch: function(oEvent) {
				var oTable = this.getView().byId("recordsTable"),
					aSelectionSet = oEvent.getParameters().selectionSet, 
					sCustomerName = aSelectionSet[0].getValue(),
					aSources = aSelectionSet[1].getSelectedItems(),
					aServiceTypes = aSelectionSet[2].getSelectedItems(),
					aSID = aSelectionSet[3].getSelectedItems(),
					oMinDate = aSelectionSet[4].getFrom(),
					oMaxDate = aSelectionSet[4].getTo(),
					aFilters = [],
					i;
					
				if (sCustomerName !== "") {
					aFilters.push(new Filter("CustomerName", "Contains", sCustomerName));
				}
				if (aSources.length > 0) {
					var aSourcesFilters = [];
					for (i = 0; i < aSources.length; i++) {
						aSourcesFilters.push(new Filter("Source", "EQ", aSources[i].getKey()));
					}
					aFilters.push(new Filter(aSourcesFilters, false));
				}
				if (aServiceTypes.length > 0) {
					var aTypesFilters = [];
					for (i = 0; i < aServiceTypes.length; i++) {
						aTypesFilters.push(new Filter("ServiceType", "EQ", aServiceTypes[i].getKey()));
					}
					aFilters.push(new Filter(aTypesFilters, false));
				}
				if (aSID.length > 0) {
					var aSIDFilters = [];
					for (i = 0; i < aSID.length; i++) {
						aSIDFilters.push(new Filter("SID", "EQ", aSID[i].getKey()));
					}
					aFilters.push(new Filter(aSIDFilters, false));
				}
				if (oMinDate !== null && oMaxDate !== null) {
					aFilters.push(new Filter("Date", "BT", oMinDate, oMaxDate));
				}
				if (aFilters.length === 0) {
					oTable.getBinding("items").filter(null);
				} else {
					oTable.getBinding("items").filter(new Filter(aFilters, true));
				}
			},
			
			onClear: function() {
				var oView = this.getView();
				oView.byId("customerNameInputFilter").setSelectedKey("");
				oView.byId("sourcesMultiBox").setSelectedItems(null);
				oView.byId("serviceTypesMultiBox").setSelectedItems(null);
				oView.byId("SIDMultiBox").setSelectedItems(null);
				oView.byId("dateSelector").setValue(null);
				oView.byId("recordsTable").getBinding("items").filter(null);
			},
			
			onFileChange: function(oEvent) {
				this._clearFields();
				var reader = new FileReader();
				reader.onload = function() {
					this.verifyInput(reader.result);
				}.bind(this);
				this._file = oEvent.getParameter("files")[0];
				reader.readAsText(this._file);
			},
			
			verifyInput: function(sInput) {
				var oUploader = this.getView().byId("fileUploader"),
						aLines = sInput.split("\n"),
						iSeparatorLine;
					
					if (aLines[0].startsWith("-")) {
						iSeparatorLine = 0;
					}  else if (aLines[1].startsWith("-")) {
						iSeparatorLine = 1;
					} else {
						oUploader.setValueState("Error");
						return;
					}
					
					var	aColumns = aLines[iSeparatorLine + 1].split("|"),
						aValues = aLines[iSeparatorLine + 3].split("|"),
						iDateColumn = -1,
						iInstanceColumn = -1,
						oDatePicker = this.getView().byId("uploadDialogDatePicker"),
						oInstanceInput = this.getView().byId("uploadDialogInstanceInput");
						oDatePicker.setDateValue(null);
						oInstanceInput.setValue("");
						oUploader.setValueState("None");
						
					for (var i = 0; i < aColumns.length; i++) {
						if (aColumns[i].includes("Date") || aColumns[i].includes("date")) {
							iDateColumn = i;
						}
						if (aColumns[i].toLowerCase().includes("instance") || aColumns[i].toLowerCase().includes("server")) {
							iInstanceColumn = i;
						}
					}
					if (iDateColumn !== -1) {
						var aDateParts = aValues[iDateColumn].split(".");
						oDatePicker.setDateValue(new Date(Date.UTC(aDateParts[2], aDateParts[1] - 1, aDateParts[0])));
						oDatePicker.setEnabled(false);
						oDatePicker.setValueState("None");
						oDatePicker.setValueStateText("");
					} else {
						oDatePicker.setEnabled(true);
						oDatePicker.setValueState("Error");
						oDatePicker.setValueStateText(this.getResourceBundle().getText("DateErrorState"));
					}
					if (iInstanceColumn !== -1) {
						var sValue = aValues[iInstanceColumn].split("_")[1];
						oInstanceInput.setValue(sValue);
						oInstanceInput.setEnabled(false);
						oInstanceInput.setValueState("None");
						oInstanceInput.setValueStateText("");
					} else {
						oInstanceInput.setEnabled(true);
						oInstanceInput.setValueState("Error");
						oInstanceInput.setValueStateText(this.getResourceBundle().getText("InstanceErrorState"));
					}
			},
			
			onEditInstancePressed: function() {
				this.getView().byId("uploadDialogInstanceInput").setEnabled(true);	
			},
			
			onEditDatePressed: function() {
				this.getView().byId("uploadDialogDatePicker").setEnabled(true);
			},
			
			onDialogCancel: function() {
				this.getView().byId("uploadDialog").close();
			},
			
			onImportDialogCancel: function() {
				this.getView().byId("importFromClipboardDialog").close();
			},
			
			onDialogUpload: function() {
				if (this._checkDialogFields()) {
					this._uploadFile();
				}
			}, 
			
			onImportDialogOk: function() {
				var oView = this.getView(),
					oTextArea = oView.byId("importDialogTextArea"),
					sImpoterdText = oTextArea.getValue(),
					sTooltipText = this.getResourceBundle().getText("CopiedFromClipboardTooltip"),
					oUploader = this.getView().byId("fileUploader");
				oView.byId("importFromClipboardDialog").close();	
				this._file = new File([sImpoterdText], "clipboardContents.txt");
				oUploader.setValue(sTooltipText);
				this.verifyInput(sImpoterdText);
			},
			
			_checkDialogFields: function() {
				var bResult = true,
					oView = this.getView(),
					oFileUploader = oView.byId("fileUploader"),
					oSourceSelect = oView.byId("sourceSelect"),
					oCustomerInput = oView.byId("customerNameInput"),
					oTypeSelect = oView.byId("serviceTypeSelect"),
					oInstanceInput = oView.byId("uploadDialogInstanceInput"),
					oDatePicker = oView.byId("uploadDialogDatePicker");
				
				if (oFileUploader.getValue() === "") {
					oFileUploader.setValueState("Error");
					bResult = false;
				} else {
					oFileUploader.setValueState("None");
				}
				
				if (oSourceSelect.getSelectedItem() === null) {
					oSourceSelect.setValueState("Error");
					bResult = false;
				} else {
					oSourceSelect.setValueState("None");
				}
				
				if (oCustomerInput.getValue() === "") {
					oCustomerInput.setValueState("Error");
					bResult = false;
				} else {
					oCustomerInput.setValueState("None");
				}
				
				if (oTypeSelect.getSelectedItem() === null) {
					oTypeSelect.setValueState("Error");
					bResult = false;
				} else {
					oTypeSelect.setValueState("None");
				}
				
				if (oInstanceInput.getValue() === "") {
					oInstanceInput.setValueState("Error");
					bResult = false;
				} else {
					oInstanceInput.setValueState("None");
				}
				
				if (oDatePicker.getValue() === "") {
					oDatePicker.setValueState("Error");
					bResult = false;
				} else {
					oDatePicker.setValueState("None");
				}
				
				return bResult;
			},
			
			_clearFields: function() {
				var oView = this.getView(),
					oSourceSelect = oView.byId("sourceSelect"),
					oCustomerInput = oView.byId("customerNameInput"),
					oTypeSelect = oView.byId("serviceTypeSelect"),
					oInstanceInput = oView.byId("uploadDialogInstanceInput"),
					oDatePicker = oView.byId("uploadDialogDatePicker");
				
				oSourceSelect.setSelectedItem(null);
				oCustomerInput.setValue("");
				oTypeSelect.setSelectedItem(null);
				oInstanceInput.setValue("");
				oDatePicker.setValue("");
			},
			
			_uploadFile: function() {
				var oFormData = new FormData(),
					oView = this.getView(),
					oSourceSelect = oView.byId("sourceSelect"),
					oCustomerInput = oView.byId("customerNameInput"),
					oTypeSelect = oView.byId("serviceTypeSelect"),
					oInstanceInput = oView.byId("uploadDialogInstanceInput"),
					oDatePicker = oView.byId("uploadDialogDatePicker"),
					oDialog = oView.byId("uploadDialog");
				oFormData.append("type", "upload");
				oFormData.append("file", this._file);
				oFormData.append("source", oSourceSelect.getSelectedItem().getKey());
				oFormData.append("customer", oCustomerInput.getValue());
				oFormData.append("serviceType", oTypeSelect.getSelectedItem().getKey());
				oFormData.append("sid", oInstanceInput.getValue());
				oFormData.append("date", oDatePicker.getValue());
				
				var xhr = new XMLHttpRequest();
				xhr.open("POST", this.SERVER_ADDRESS);
				xhr.onreadystatechange = function() {
					console.log("On ready state change. Code: " + xhr.readyState +  " Time: " + Date.now());
					console.log(xhr.status);
					if (xhr.readyState === 4) {
						oDialog.setBusy(false);
						this.getView().byId("fileUploader").clear();
						this._clearFields();
						oDialog.close();
						setTimeout(function() {
							this.getOwnerComponent().getModel().refresh();
						}.bind(this), 5000);
					}
				}.bind(this);
				xhr.onloadend = function() {
					this.getOwnerComponent().getModel().refresh();
				}.bind(this);
				oDialog.setBusy(true);
				xhr.send(oFormData);
			},
			
			_deleteData: function(sId) {
				var oFormData = new FormData(),
					oTable = this.getView().byId("recordsTable"),
					xhr = new XMLHttpRequest();
				
				oFormData.append("type", "delete");
				oFormData.append("id", sId);
				xhr.open("POST", this.SERVER_ADDRESS);
				xhr.onreadystatechange = function() {
					console.log("On ready state change. Code: " + xhr.readyState +  " Time: " + Date.now());
					console.log(xhr.status);
					if (xhr.readyState === 4) {
						oTable.setBusy(false);
						setTimeout(function() {
								this.getOwnerComponent().getModel().refresh();
						}.bind(this), 1000);
					}
				}.bind(this);
				xhr.onloadend = function() {
					this.getOwnerComponent().getModel().refresh();
				}.bind(this);
				oTable.setBusy(true);
				xhr.send(oFormData);
			} 
		});
	}
);