sap.ui.define([
	"sap/ui/core/format/DateFormat",
	"sap/ui/core/Locale"
	], function(DateFormat, Locale) {
	"use strict";
	
	return {
		mediumDate: function(oDate) {
			var oLocale = new Locale("en_US");
			var oDateFormat = DateFormat.getDateInstance({
				style: "medium",
				UTC: true
			}, oLocale);
			return oDateFormat.format(oDate);
		},
		
		timeFormat: function(oTime) {
			if (oTime !== null && oTime.ms !== undefined) {
				var timeZoneOffset = new Date(0).getTimezoneOffset() * 60 * 1000;
				return new Date(oTime.ms + timeZoneOffset);
			}
		},
		
		timeToString: function(oTime) {
			if (oTime !== null && oTime !== undefined && oTime.ms !== undefined) {
				var timeZoneOffset = new Date(0).getTimezoneOffset() * 60 * 1000, 
					oDate = new Date(oTime.ms + timeZoneOffset);
				return oDate.toTimeString().split(" ")[0];
			}
		}
	};
});